> 五分钟 Markdown —— [连享会](https://www.lianxh.cn)

&emsp; 

> &#x1F449;  你可以点击右上角【Fork】按钮，把本项目 Fork 到你的码云主页下。定期强制同步，可以实现与本项目主页的同步更新。 你的码云主页下可以自建很多新项目。

&emsp;
- 五分钟 Markdown 视频：[新浪微博](https://weibo.com/tv/v/IzkIK5mHr?fid=1034:4484204327796746)；[短书直播](https://lianxh.duanshu.com/#/brief/video/1ad258af4a5b429d8edf2f20e843da2b)
- [Wikis](https://gitee.com/arlionn/md/wikis/Home) 里有详细的教程
- [幻灯片](https://gitee.com/arlionn/md/raw/master/%E8%BF%9E%E7%8E%89%E5%90%9B-%E4%BA%94%E5%88%86%E9%92%9FMarkdown-%E5%B9%BB%E7%81%AF%E7%89%87.pdf) [曲奇云盘版](https://quqi.gblhgk.com/s/880197/hUkcZsC5wH4eWAgF) 
- 用 Markdown 写一个幻灯片
  - 在谷歌浏览器中输入 [web.marp.app](web.marp.app)
  - 单击 | [「五分钟Markdown-幻灯片-原始文档」](https://gitee.com/arlionn/md/blob/master/%E4%BA%94%E5%88%86%E9%92%9FMarkdown-%E5%8E%9F%E5%A7%8B%E6%96%87%E6%A1%A3.do)，复制里面的文字。(或者，点击 [这里](https://gitee.com/arlionn/md/attach_files/352673/download) 下载)
  - 将上述文档的内容贴入 [web.marp.app](web.marp.app)，即可看到漂亮的幻灯片
  - 依次点击左上角的「**蓝色三角形图标**」&rarr;「**Print / Export to PDF**」 即可输出 PDF 格式的幻灯片。 
- Marp 幻灯片展示
  - 连玉君，[我的甲壳虫-直播课-幻灯片](https://gitee.com/arlionn/paper101/wikis/%E5%B9%BB%E7%81%AF%E7%89%87.md?sort_id=1985074)
  - 连玉君，[动态面板数据模型-幻灯片](https://gitee.com/arlionn/Live/tree/master/%E8%BF%9E%E7%8E%89%E5%90%9B-%E5%8A%A8%E6%80%81%E9%9D%A2%E6%9D%BF%E6%A8%A1%E5%9E%8B)


&emsp;

&emsp;


> ##### 导航： 📍 [连享会主页](https://www.lianxh.cn)  &ensp;  📍 [知乎专栏](https://www.zhihu.com/people/arlionn/)  &ensp;  📍 [公众号推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) 


> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)

&emsp;

&emsp; 

> #### &#x23E9;  [连享会·直播 - DSGE 专题](https://gitee.com/arlionn/DSGE)     
> **线上直播 4 天**：2020.9.19-20; 9.26-27  
> **主讲嘉宾**：朱传奇 (中山大学)  
>    &emsp;    
> **课程主页**：<https://gitee.com/arlionn/DSGE> 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-DSGE-海报600.png)

&emsp;

![](https://images.gitee.com/uploads/images/2020/0816/231423_d0424d1b_1522177.png)

&emsp;

## 相关课程

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  
> <img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[DSGE 专题](https://gitee.com/arlionn/DSGE)** | 朱传奇| [线上直播 4 天](https://www.lianxh.cn/news/c2f757a200474.html) <br> 2020年9月 19-20日; 26-27日  |
| &#x2B55; **[Stata数据清洗](https://lxh.360dhf.cn/live/detail/4114)** | 游万海| [直播, 2 小时](https://www.lianxh.cn/news/f785de82434c1.html) <br> 2020.7.21 |
| **[空间计量系列](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)** | 范巧    | [空间全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), [空间权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) <br> [空间动态面板](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3), [空间DID](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |


> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh底部推文海报.png)


&emsp;

&emsp;
 
## 课程速看   

&emsp;

> ### [连享会直播：Stata数据清洗 (已上线)](https://lxh.360dhf.cn/live/detail/4114)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/游万海-数据清洗-海报.png)


&emsp;


&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。


---
